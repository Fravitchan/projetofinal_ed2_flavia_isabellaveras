#include <stdio.h>
#include <time.h>
#include <string.h>
#include <stdlib.h>
#include "ArvoreAVL.h"
#include "ArvoreLLRB.H"

int main()
{
    int x;
    ArvAvl *raiz;
    ArvLLRB *raiz2;
    double AVL, LLRB, tInicio, tFim; //Variáveis tInicio e tFim utilizadas para cálculo do tempo de execução.

    //Alocação de memória para a estrura que armazena as informações do arquivo CSV.
    struct cadastroPessoa *Funcionario = (struct cadastroPessoa*) malloc(sizeof(struct cadastroPessoa));

    tInicio = clock(); //Contagem do tempo.

    raiz = cria_ArvAvl();

      x = leituraCSV1(raiz, Funcionario);
    if(x){
        printf("\n\t-> Informacoes inseridas com sucesso.\n");
    }else{
        printf("\n\t-> Erro na insercao.");
    }


    x=0;
    //Exibição de elementos específicos.
    printf("\n\t\t=========================== AVL =====================");
    printf("\n\n\n\t==========================1==========================\n\n\n");
    x = consulta_ArvAvl(raiz, 26875);
    printf("\n\n\n\t==========================2==========================\n\n\n");
    x = consulta_ArvAvl(raiz, 77944);
    printf("\n\n\n\t==========================3==========================\n\n\n");
    x = consulta_ArvAvl(raiz, 8838);
    printf("\n\n\n\t==========================4==========================\n\n\n");
    x = consulta_ArvAvl(raiz, 53817);
    printf("\n\n\n\t==========================5==========================\n\n\n");
    x = consulta_ArvAvl(raiz, 37792);
    printf("\n\n\n\t==========================6==========================\n\n\n");
    x = consulta_ArvAvl(raiz, 92281);
    printf("\n\n\n\t==========================7==========================\n\n\n");
    x = consulta_ArvAvl(raiz, 84858);
    printf("\n\n\n\t==========================8==========================\n\n\n");
    x = consulta_ArvAvl(raiz, 91648);
    printf("\n\n\n\t==========================9==========================\n\n\n");
    x = consulta_ArvAvl(raiz, 81073);
    printf("\n\n\n\t==========================10==========================\n\n\n");
    x = consulta_ArvAvl(raiz, 16450);



    printf("\n\n\n\n\t==========================POS-EXCLUSAO==========================\n\n\n");

    printf("\n\t==========================1==========================\n\n\n");
    x = mostraPosExclusao_ArvAvl(raiz, 26875);
    printf("\n\n\n\t==========================2==========================\n\n\n");
    x = mostraPosExclusao_ArvAvl(raiz, 77944);
    printf("\n\n\n\t==========================3==========================\n\n\n");
    x = mostraPosExclusao_ArvAvl(raiz, 8838);
    printf("\n\n\n\t==========================4==========================\n\n\n");
    x = mostraPosExclusao_ArvAvl(raiz, 53817);
    printf("\n\n\n\t==========================5==========================\n\n\n");
    x = mostraPosExclusao_ArvAvl(raiz, 37792);
    printf("\n\n\n\t==========================6==========================\n\n\n");
    x = mostraPosExclusao_ArvAvl(raiz, 92281);
    printf("\n\n\n\t==========================7==========================\n\n\n");
    x = mostraPosExclusao_ArvAvl(raiz, 84858);
    printf("\n\n\n\t==========================8==========================\n\n\n");
    x = mostraPosExclusao_ArvAvl(raiz, 91648);
    printf("\n\n\n\t==========================9==========================\n\n\n");
    x = mostraPosExclusao_ArvAvl(raiz, 81073);
    printf("\n\n\n\t==========================10==========================\n\n\n");
    x = mostraPosExclusao_ArvAvl(raiz, 16450);
    printf("\n\n\n\t======================================================\n\n\n");

    libera_ArvAvl(raiz);

    tFim = clock(); //Fim da contagem do tempo.

    AVL = ((tFim - tInicio) / (CLOCKS_PER_SEC / 1000)); //Tempo final de execução da árvore.

    tFim =0;
    tInicio =0;

    printf("\n\n\tArvore AVL: %.2f\n\n\n", AVL);
    system("pause");

    tInicio = clock(); //Contagem do tempo.


    raiz2 = cria_ArvLLRB();

      leituraCSV1(raiz2, Funcionario);


    x=0;
    //Exibição de elementos específicos.
    printf("\n\t\t=========================== RUBRO NEGRA/LLRB =====================");
    printf("\n\n\n\t==========================1==========================\n\n\n");
    x = consulta_ArvLLRB(raiz2, 26875);
    printf("\n\n\n\t==========================2==========================\n\n\n");
    x = consulta_ArvLLRB(raiz2, 77944);
    printf("\n\n\n\t==========================3==========================\n\n\n");
    x = consulta_ArvLLRB(raiz2, 8838);
    printf("\n\n\n\t==========================4==========================\n\n\n");
    x = consulta_ArvLLRB(raiz2, 53817);
    printf("\n\n\n\t==========================5==========================\n\n\n");
    x = consulta_ArvLLRB(raiz2, 37792);
    printf("\n\n\n\t==========================6==========================\n\n\n");
    x = consulta_ArvLLRB(raiz2, 92281);
    printf("\n\n\n\t==========================7==========================\n\n\n");
    x = consulta_ArvLLRB(raiz2, 84858);
    printf("\n\n\n\t==========================8==========================\n\n\n");
    x = consulta_ArvLLRB(raiz2, 91648);
    printf("\n\n\n\t==========================9==========================\n\n\n");
    x = consulta_ArvLLRB(raiz2, 81073);
    printf("\n\n\n\t==========================10==========================\n\n\n");
    x = consulta_ArvLLRB(raiz2, 16450);



    printf("\n\n\n\n==========================POS-EXCLUSAO==========================\n\n\n");

    printf("\n==========================1==========================\n\n\n");
    x = mostraPosExclusao_ArvLLRB(raiz2, 26875);
    printf("\n\n\n==========================2==========================\n\n\n");
    x = mostraPosExclusao_ArvLLRB(raiz2, 77944);
    printf("\n\n\n==========================3==========================\n\n\n");
    x = mostraPosExclusao_ArvLLRB(raiz2, 8838);
    printf("\n\n\n==========================4==========================\n\n\n");
    x = mostraPosExclusao_ArvLLRB(raiz2, 53817);
    printf("\n\n\n==========================5==========================\n\n\n");
    x = mostraPosExclusao_ArvLLRB(raiz2, 37792);
    printf("\n\n\n==========================6==========================\n\n\n");
    x = mostraPosExclusao_ArvLLRB(raiz2, 92281);
    printf("\n\n\n==========================7==========================\n\n\n");
    x = mostraPosExclusao_ArvLLRB(raiz2, 84858);
    printf("\n\n\n==========================8==========================\n\n\n");
    x = mostraPosExclusao_ArvLLRB(raiz2, 91648);
    printf("\n\n\n==========================9==========================\n\n\n");
    x = mostraPosExclusao_ArvLLRB(raiz2, 81073);
    printf("\n\n\n==========================10==========================\n\n\n");
    x = mostraPosExclusao_ArvLLRB(raiz2, 16450);
    printf("\n\n\n======================================================\n\n\n");

    libera_ArvLLRB(raiz2);

    tFim = clock(); //Fim da contagem de tempo.

    LLRB = ((tFim - tInicio) / (CLOCKS_PER_SEC / 1000)); //Tempo final de execução da árvore.

    printf("\n\n\tArvore LLRB: %.2f\n\n",LLRB);


    system("pause");

    system("cls");
    //Comparação final.
    printf("\n\n\n\t========================== RELATORIO FINAL ==========================\n\n");

    printf("\t\t=== TEMPO DE EXECUCAO DE CADA ARVORE ===");
    printf("\n\tAVL: %.2f",AVL);
    printf("\n\tLLRB (Rubro Negra): %.2f",LLRB);

    printf("\n\n\n\t\n\n\n");




}
