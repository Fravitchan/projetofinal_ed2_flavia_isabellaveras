typedef struct NO *ArvAvl;

//Estrutura que armazena os dados do arquivo CSV.
typedef struct cadastroPessoa{
    int cod;
    char nome[50];
    int idade;
    char empresa[50];
    char departamento[50];
    float salario;
}dados;

//Cria��o da �rvore AVL.
ArvAvl *cria_ArvAvl();

//Fun��o libera �rvore.
void libera_ArvAvl(ArvAvl *raiz);

int vazia_ArvAvl(ArvAvl *raiz);

//Fun��o que realiza a leitura do arquivo CSV.
int leituraCSV1(ArvAvl *raiz, struct cadastroPessoa *info);

//Fun��o que realiza consulta das informa��es armazenadas na �rvore.
int consulta_ArvAvl(ArvAvl *raiz, int valor);

//Fun��o que exibe o conte�do ap�s a exclus�o de informa��es.
int mostraPosExclusao_ArvAvl(ArvAvl *raiz, int valor);
