#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include "ArvoreLLRB.H"
#define RED 1
#define BLACK 0

//Informacoes tabela CSV


//Função LLRB
typedef struct NO{
    dados2 info;
    int alt; //inclui a altura da sub-arvore.
    struct NO *esq;
    struct NO *dir;
    int cor;
};

ArvLLRB *cria_ArvLLRB(){
    ArvLLRB *raiz = (ArvLLRB*) malloc(sizeof(ArvLLRB));
    if(raiz != NULL){
        *raiz = NULL;
    }
    return raiz;
}

int cor(struct NO *H){

    if(H == NULL){
        return BLACK;
    }else{
        return H->cor;
    }
}

void trocaCor(struct NO *H){
    H->cor = !H->cor;
    if(H->esq != NULL){
        H->esq->cor = !H->esq->cor;
    }
    if(H->dir != NULL){
        H->dir->cor = !H->dir->cor;
    }
}

struct NO *rotacionaEsquerda(struct NO *A){
    struct NO *B = A->dir;
    A->dir = B->esq;
    B->esq = A;
    B->cor = A->cor;
    A->cor = RED;
    return B;
};

struct NO *rotacionaDireita(struct NO *A){
    struct NO *B = A->esq;
    A->esq = B->dir;
    B->dir = A;
    B->cor = A->cor;
    A->cor = RED;
    return B;
};

struct NO *move2EsqRED(struct NO *H){
    trocaCor(H);
    if(cor(H->dir->esq) == RED){
        H->dir = rotacionaDireita(H->dir);
        H = rotacionaEsquerda(H);
        trocaCor(H);
    }
    return H;
};

struct NO *move2DirRED(struct NO *H){
    trocaCor(H);
    if(cor(H->esq->esq) == RED){
        H = rotacionaDireita(H);
        trocaCor(H);
    }
    return H;
};

struct NO *balancear(struct NO *H){
    if(cor(H->dir) == RED){
        H = rotacionaEsquerda(H);
    }
    if(H->esq != NULL && cor(H->dir) == RED && cor(H->esq->esq) == RED){
        H = rotacionaDireita(H);
    }
    if(cor(H->esq) == RED && cor(H->dir) == RED){
        trocaCor(H);
    }
    return H;
};


void libera_ArvLLRB(ArvLLRB *raiz){
    if(raiz == NULL){
        return;
    }
    libera_NO_ArvLLRB(*raiz);
    printf("\n\n\tArvore Limpa\n");
    free(raiz);
}


void libera_NO_ArvLLRB(struct NO *no){
    if(no == NULL){
        return;
    }
    libera_NO_ArvLLRB(no->esq);
    libera_NO_ArvLLRB(no->dir);
    free(no);
    no = NULL;
}

struct NO *insereNO(struct NO *H, dados2 *info, int *resp){
    if(H == NULL){
        struct NO *novo;
        novo = (struct NO*) malloc(sizeof(struct NO));
        if(novo == NULL){
            *resp = 0;
            return NULL;
        }
        novo->info = *info;
        novo->cor = RED;
        novo->dir = NULL;
        novo->esq = NULL;
        *resp = 1;
        return novo;
    }
    if(info->cod == H->info.cod){
        *resp = 0;
    }
    else{
        if(info->cod < H->info.cod){
            H->esq = insereNO(H->esq, info, resp);
        }else{
            H->dir = insereNO(H->dir, info, resp);
        }
    }
    if(cor(H->dir) == RED && cor(H->esq) == BLACK){
        H = rotacionaEsquerda(H);
    }
    if(cor(H->esq) == RED && cor(H->esq->esq) == RED){
        H = rotacionaDireita(H);
    }
    if(cor(H->esq) == RED && cor(H->dir) == RED){
        trocaCor(H);
    }
    return H;
};

int insere_ArvLLRB(ArvLLRB *raiz, dados2 *info){
    int resp;
    //fun��o respons�vel pela busca do local de inser��o do n�
    *raiz = insereNO(*raiz, info, &resp);
    if((*raiz) != NULL){
        (*raiz)->cor = BLACK;
    }
    return resp;
}




struct NO *removeNO(struct NO *H, int valor){
    if(valor < H->info.cod){
        if(cor(H->esq) == BLACK && cor(H->esq->esq) == BLACK){
            H = move2EsqRED(H);
        }
        H->esq = removeNO(H->esq, valor);
    }else{
        if(cor(H->esq) == RED){
            H = rotacionaDireita(H);
        }
        if(valor == H->info.cod && (H->dir == NULL)){
            free(H);
            return NULL;
        }
        if(cor(H->dir) == BLACK && cor(H->dir->esq) == BLACK){
            H = move2DirRED(H);
        }
        if(valor == H->info.cod){
            struct NO *x = menor(H->dir);
            H->info = x->info;
            H->dir = menor(H->dir);
        }else{
            H->dir = removeNO(H->dir, valor);
        }
    }
    return balancear(H);
};

int remove_ArvLLRB(ArvLLRB *raiz, int valor){
    struct NO *H = *raiz;

    //fun��o responsavel pela busca pelo n� a ser removido
    *raiz = removeNO(H, valor);

    if(*raiz != NULL){
        (*raiz)->cor = BLACK;
    }
}



struct NO *removeMenor(struct NO *H){
    if(H->esq == NULL){
        free(H);
        return NULL;
    }
    if(cor(H->esq) == BLACK && cor(H->esq->esq) == BLACK){
        H = move2EsqRED(H);
    }
    H->esq = removeMenor(H->esq);
    return balancear(H);
};

int consulta_ArvLLRB(ArvLLRB *raiz, int valor){

    if(raiz == NULL){
        return 0;
    }

    struct NO *atual = *raiz;


    while(atual){

        if(valor == atual->info.cod){
            printf("\n\t\t-> NO PAI <-\n");
            printf("\tID: %d",atual->info.cod);
            printf("\n\tNome: %s\n", atual->info.nome);



            if(atual->esq != NULL){
                printf("\n\t\t==> FILHO ESQUERDO");
                printf("\n\tID: %d\n", atual->esq->info.cod);
                printf("\tNome: %s",atual->esq->info.nome);
                printf("\n\tIdade: %d",atual->esq->info.idade);
            }else{
                printf("\n\tNao possui filho esquerdo\n");
            }
            if(atual->dir != NULL){
                printf("\n\t\t==> FILHO DIREITO");
                printf("\n\tID: %d\n", atual->dir->info.cod);
                printf("\tNome: %s",atual->dir->info.nome);
                printf("\n\tIdade: %d",atual->dir->info.idade);
            }else{
                printf("\n\tNao possui filho direito\n");
            }

            if(atual->dir == NULL && atual->esq == NULL)
            {
                printf("\n\tEste NO e uma folha.\n");
            }

            if( atual->info.cod == 53817 ||  //Remocao filho direito
                atual->info.cod == 37792 ||
                atual->info.cod == 84858 ||
                atual->info.cod == 81073 ||
                atual->info.cod == 16450   )
            {
                if(atual->dir != NULL)
                    {
                        remove_ArvLLRB(raiz, atual->dir->info.cod);
                    }else
                    {
                        printf("\n\tNAO possui filho para ser removido");
                    }
            }
             if( atual->info.cod == 91648 ||  //Remocao filho esquerdo
                 atual->info.cod == 92281 ||
                 atual->info.cod == 8838 ||
                 atual->info.cod == 77944 ||
                 atual->info.cod == 26875  )
            {
                if(atual->dir != NULL)
                    {
                        remove_ArvLLRB(raiz, atual->esq->info.cod);
                    }else
                    {
                        printf("\n\tNAO possui filho para ser removido");
                    }
            }
        }

        if(valor > atual->info.cod){
            atual = atual->dir;
        }else{
            atual = atual->esq;
        }


    }
}

int mostraPosExclusao_ArvLLRB(ArvLLRB *raiz, int valor){
  if(raiz == NULL){
        return 0;
    }

    struct NO *atual = *raiz;


    while(atual){

        if(valor == atual->info.cod){
            printf("\n\t\t-> NO PAI <-\n");
            printf("\tID: %d",atual->info.cod);
            printf("\n\tNome: %s\n", atual->info.nome);



            if(atual->esq != NULL){
                printf("\n\t\t==> FILHO ESQUERDO");
                printf("\n\tID: %d\n", atual->esq->info.cod);
                printf("\tNome: %s",atual->esq->info.nome);
                printf("\n\tIdade: %d",atual->esq->info.idade);
            }else{
                printf("\n\tNao possui filho esquerdo\n");
            }
            if(atual->dir != NULL){
                printf("\n\t\t==> FILHO DIREITO");
                printf("\n\tID: %d\n", atual->dir->info.cod);
                printf("\tNome: %s",atual->dir->info.nome);
                printf("\n\tIdade: %d",atual->dir->info.idade);
            }else{
                printf("\n\tNao possui filho direito\n");
            }

            if(atual->dir == NULL && atual->esq == NULL)
            {
                printf("\n\tEsse NO e uma folha.\n");
            }


        }

        if(valor > atual->info.cod){
            atual = atual->dir;
        }else{
            atual = atual->esq;
        }


    }
}

int leituraCSVArvLLRB(ArvLLRB *raiz, struct cadastroPessoa2 *info){
    int i=0, x;
    struct Pessoa *aux;
    char texto[20000];
    FILE *arquivo = fopen("massaDados.csv", "r");

    if(arquivo == NULL){
        printf("\nFalha na abertura do arquivo!\n\n");
        system("pause");
        return 0;
    }

    while(EOF){
        fgets(texto, 20000, arquivo);
        info->cod = atoi(strtok(texto, ";"));
        strcpy(info->nome, strtok(NULL, ";"));
        info->idade = atoi(strtok(NULL, ";"));
        strcpy(info->empresa, strtok(NULL, ";"));
        strcpy(info->departamento, strtok(NULL, ";"));
        info->salario = atof(strtok(NULL, "\n"));
        i++;
        aux = info;

        x = insere_ArvLLRB(raiz, info);
        if(i == 15000){
            printf("\nQuantidade de dados inseridos: %d\n\n", i);
            fclose(arquivo);
            return 1;
        }
    }

    system("pause");

    fclose(arquivo);
    return 1;
}



